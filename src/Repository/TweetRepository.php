<?php

namespace App\Repository;

use App\Entity\Tweet;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tweet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tweet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tweet[]    findAll()
 * @method Tweet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TweetRepository extends ServiceEntityRepository
{
    /**
     * @var ResultSetMapping
     */
    private $rsm;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tweet::class);

        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Tweet::class, 't');
        $rsm->addFieldResult('t', 'id', 'id');
        $rsm->addFieldResult('t', 'message', 'message');
        $rsm->addFieldResult('t', 'created_at', 'createdAt');

        $rsm->addEntityResult(User::class, 'u');
        $rsm->addJoinedEntityResult(User::class , 'u', 't', 'author');
        $rsm->addFieldResult('u', 'username', 'username');
        $rsm->addFieldResult('u', 'userId', 'id');

        $rsm->addScalarResult('nbComments', 'nbComments');

        $this->rsm = $rsm;
    }

    /**
     * Find all Tweets created by the given User
     *
     * @param User $author
     * @return Tweet[] Returns an array of Tweet objects
     */
    public function findByAuthor(User $author)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.author = :val')
            ->setParameter('val', $author)
            ->orderBy('t.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }


    /**
     * Find all Tweets immediate children to the given Tweet
     *
     * @param Tweet $parent
     * @return Tweet[] Returns an array of Tweet objects
     */
    public function findByParent(Tweet $parent)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.parent = :val')
            ->setParameter('val', $parent)
            ->orderBy('t.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }


    /**
     * Fetch all parent Tweets with their comments count and author,
     * @param bool $optimize - pass false tu run a trivial query (debugging purpose only)
     * @return Tweet[]
     */
    public function findRootTweets($optimize = true) {

        if ($optimize) {
            $rawResults = $this->_em->createNativeQuery('
                SELECT t.*, (
                    SELECT COUNT(*) as nbComments
                    FROM tweet
                    WHERE parent_id = t.id
                ) as nbComments,
                u.id as userId,
                u.username
                FROM tweet as t
                INNER JOIN user as u
                  ON u.id = t.author_id
                WHERE parent_id is NULL
                ORDER BY t.created_at DESC
            ', $this->rsm)
         ->getResult();

            // TODO this loop is not really necessary.
            //  It can be even more optimized by reading raw results from the template directly
            return array_map(function($result) {
                /**
                 * @var Tweet $tweet
                 */
                $tweet = $result[0];
                $tweet->setNbComments($result['nbComments']);
                return $tweet;
            }, $rawResults);


        } else {

            return $this->createQueryBuilder('t')
                ->select('t, u')
                ->andWhere('t.parent is NULL')
                ->join('t.author', 'u')
                ->orderBy('t.createdAt', 'DESC')
                ->getQuery()
                ->getResult();

        }
    }


    /*
    public function findOneBySomeField($value): ?Tweet
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
