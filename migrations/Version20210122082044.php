<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210122082044 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add "Like" many-to-many relation between User and Tweet';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_like_tweet (user_id INT NOT NULL, tweet_id INT NOT NULL, INDEX IDX_DFA4F163A76ED395 (user_id), INDEX IDX_DFA4F1631041E39B (tweet_id), PRIMARY KEY(user_id, tweet_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_like_tweet ADD CONSTRAINT FK_DFA4F163A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_like_tweet ADD CONSTRAINT FK_DFA4F1631041E39B FOREIGN KEY (tweet_id) REFERENCES tweet (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user_like_tweet');
    }
}
