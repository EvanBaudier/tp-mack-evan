# TP Symfony




## Working with Database

Setup the database :

```sh 
php bin/console doctrine:database:create
```

Load migrations : 
```sh 
php bin/console doctrine:migrations:migrate
```

Load the fixtures (first time):
```sh 
php bin/console doctrine:fixtures:load
```

Reload the fixtures :
```sh 
php bin/console doctrine:schema:drop --force && php bin/console doctrine:schema:update --force && php bin/console doctrine:fixtures:load -n
```

Destroy the database :

```sh 
php bin/console doctrine:database:drop --force
```